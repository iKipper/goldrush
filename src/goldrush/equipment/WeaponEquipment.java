package goldrush.equipment;

import org.bukkit.inventory.ItemStack;

public abstract class WeaponEquipment extends Equipment {

	public WeaponEquipment(ItemStack item, String name) {
		super(item, name);
	}
}