package goldrush.equipment.armor;

import goldrush.equipment.ArmourEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class DiamondLeggings extends ArmourEquipment {
	private static ItemStack i;
	static {
		i = new ItemStack(Material.DIAMOND_LEGGINGS);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.GOLD + "Diamond Leggings");
		i.setItemMeta(im);
	}

	public DiamondLeggings() {
		super(i, "Diamond Leggings");
		this.setSlot(16);
		this.setPrice(50);
	}
}
