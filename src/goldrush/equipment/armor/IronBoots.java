package goldrush.equipment.armor;

import goldrush.equipment.ArmourEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class IronBoots extends ArmourEquipment {
	private static ItemStack i;
	static {
		i = new ItemStack(Material.IRON_BOOTS);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.GOLD + "Iron Boots");
		i.setItemMeta(im);
	}

	public IronBoots() {
		super(i, "Iron Boots");
		this.setSlot(12);
		this.setPrice(25);
	}
}