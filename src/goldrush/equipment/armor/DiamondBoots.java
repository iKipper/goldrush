package goldrush.equipment.armor;

import goldrush.equipment.ArmourEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class DiamondBoots extends ArmourEquipment {
	private static ItemStack i;
	static {
		i = new ItemStack(Material.DIAMOND_BOOTS);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.GOLD + "Diamond Boots");
		i.setItemMeta(im);
	}

	public DiamondBoots() {
		super(i, "Diamond Boots");
		this.setSlot(17);
		this.setPrice(50);
	}
}
