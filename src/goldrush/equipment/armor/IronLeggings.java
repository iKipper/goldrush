package goldrush.equipment.armor;

import goldrush.equipment.ArmourEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class IronLeggings extends ArmourEquipment {
	private static ItemStack i;
	static {
		i = new ItemStack(Material.IRON_LEGGINGS);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.GOLD + "Iron Leggings");
		i.setItemMeta(im);
	}

	public IronLeggings() {
		super(i, "Iron Leggings");
		this.setSlot(11);
		this.setPrice(25);
	}
}