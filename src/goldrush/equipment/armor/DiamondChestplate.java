package goldrush.equipment.armor;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import goldrush.equipment.ArmourEquipment;

public class DiamondChestplate extends ArmourEquipment {
	private static ItemStack i;
	static {
		i = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.GOLD + "Diamond Chestplate");
		i.setItemMeta(im);
	}

	public DiamondChestplate() {
		super(i, "Diamond Chestplate");
		this.setSlot(15);
		this.setPrice(50);
	}
}
