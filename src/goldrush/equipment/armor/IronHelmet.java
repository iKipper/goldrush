package goldrush.equipment.armor;

import goldrush.equipment.ArmourEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class IronHelmet extends ArmourEquipment {
	private static ItemStack i;
	static {
		i = new ItemStack(Material.IRON_HELMET);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.GOLD + "Iron Helmet");
		i.setItemMeta(im);
	}

	public IronHelmet() {
		super(i, "Iron Helmet");
		this.setSlot(9);
		this.setPrice(25);
	}
}