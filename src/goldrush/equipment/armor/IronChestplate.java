package goldrush.equipment.armor;

import goldrush.equipment.ArmourEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class IronChestplate extends ArmourEquipment {
	private static ItemStack i;
	static {
		i = new ItemStack(Material.IRON_CHESTPLATE);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.GOLD + "Iron Chestplate");
		i.setItemMeta(im);
	}

	public IronChestplate() {
		super(i, "Iron Chestplate");
		this.setSlot(10);
		this.setPrice(25);
	}
}