package goldrush.equipment.armor;

import goldrush.equipment.ArmourEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class DiamondHelmet extends ArmourEquipment {
	private static ItemStack i;
	static {
		i = new ItemStack(Material.DIAMOND_HELMET);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.GOLD + "Diamond Helmet");
		i.setItemMeta(im);
	}

	public DiamondHelmet() {
		super(i, "Diamond Helmet");
		this.setSlot(14);
		this.setPrice(50);
	}
}