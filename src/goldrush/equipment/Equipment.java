package goldrush.equipment;

import org.bukkit.inventory.ItemStack;

public abstract class Equipment {

	private String name;
	private ItemStack item;
	private int price, slot;

	/**
	 * Creates new equipment with the right item and name The default values for
	 * the other fiels are: minGold: 1 maxGold: 1 price: 1 To change these
	 * values, use the setters
	 * 
	 * The can not be 2 pieces of equipment with the same name!
	 * 
	 * @param item
	 *            the target item
	 * @param name
	 *            name of the item
	 */
	public Equipment(ItemStack item, String name) {
		this.name = name;
		this.item = item;
		price = 1;
		slot = -1;
	}

	/**
	 * Returns the name of the {@link Equipment}
	 * 
	 * @return The name of the equipment
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the {@link ItemStack} of the {@link Equipment}
	 * 
	 * @return The ItemStack of the equipment
	 */
	public ItemStack getItem() {
		return item;
	}
	/**
	 * Returns the amount of nuggets it will cost someone to buy this piece of
	 * {@link Equipment}
	 * 
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * Sets the amount of nuggets it will cost someone to buy this piece of
	 * {@link Equipment}
	 * 
	 * @param price
	 *            the price
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	public int getSlot() {
		return slot;
	}

	public void setSlot(int slot) {
		this.slot = slot;
	}

	/**
	 * Returns true if the item given matches the item of the equipment. Based
	 * upon the fact that there may not be two pieces of equipment with the same
	 * name the comparison is based on the displaynames of the items. If
	 * therefore the item given has the same displayname as the equipment, the
	 * method will still return true, even if none of the other properties of
	 * the items match, such as the material.
	 * 
	 * @param i
	 *            The item to compare
	 * @return True if the item matches. False if the item doesn't match.
	 */
	public boolean itemMatches(ItemStack i) {
		if (i != null) {
			if (i.getItemMeta() != null) {
				if (i.getItemMeta().getDisplayName() != null) {
					if (i.getItemMeta().getDisplayName().equals(this.item.getItemMeta().getDisplayName())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public boolean equals(Object obj) {
		if (obj instanceof Equipment) {
			Equipment other = (Equipment) obj;
			if (other.getName().equals(getName())) {
				return true;
			}
		}
		return false;
	}
}