package goldrush.equipment;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;

public class EquipmentManager {

	private boolean isAccepting;
	private List<Equipment> equipment;

	public EquipmentManager() {
		isAccepting = true;
		equipment = new ArrayList<Equipment>();
	}

	public void registerEquipment(Equipment e) {
		if (isAccepting) {
			if (!equipment.contains(e)) {
				equipment.add(e);
			}
		}
	}

	public Equipment getEquipment(ItemStack item) {
		for (Equipment equip : equipment) {
			if (equip.itemMatches(item)) {
				return equip;
			}
		}
		return null;
	}

	public Equipment getEquipment(String name) {
		for (Equipment equip : equipment) {
			if (equip.getName().equals(name)) {
				return equip;
			}
		}
		return null;
	}

	public List<Equipment> getEquipment() {
		return equipment;
	}

}