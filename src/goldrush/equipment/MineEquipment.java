package goldrush.equipment;

import java.util.Random;

import org.bukkit.inventory.ItemStack;

public abstract class MineEquipment extends Equipment {

	private int minGold, maxGold;

	public MineEquipment(ItemStack item, String name) {
		super(item, name);
		minGold = 1;
		maxGold = 1;
	}

	/**
	 * Returns a random number amount of gold based on the minGold and maxGold
	 * integers
	 * 
	 * @return a random integer between the minGold and maxGold
	 */
	public int getRandomGold() {
		int range = Math.abs(maxGold - minGold);
		Random rand = new Random();
		return rand.nextInt(range + 1) + minGold;
	}

	/**
	 * Returns the minimum amount of gold mined by this piece of
	 * {@link Equipment}
	 * 
	 * @return the minimum amount of gold
	 */
	public int getMinGold() {
		return minGold;
	}

	/**
	 * Used to set the minimum amount of gold that can be mined using this piece
	 * of {@link Equipment}. If the minGold is set to 5, every time a block is
	 * mined using this equipment at least 5 golden nuggets will be given to the
	 * player.
	 * 
	 * @param minGold
	 *            the minimum amount of gold
	 */
	public void setMinGold(int minGold) {
		this.minGold = minGold;
	}

	/**
	 * Returns the maximum amount of gold mined by this piece of
	 * {@link Equipment}
	 * 
	 * @return the maximum amount of gold
	 */
	public int getMaxGold() {
		return maxGold;
	}

	/**
	 * Used to set the maximum amount of gold that can be mined using this piece
	 * of {@link Equipment}. If the maxGold is set to 5, every time a block is
	 * mined using this equipment at most 5 golden nuggets will be given to the
	 * player.
	 * 
	 * @param maxGold
	 *            the maximum amount of gold
	 */
	public void setMaxGold(int maxGold) {
		this.maxGold = maxGold;
	}

}
