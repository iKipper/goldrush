package goldrush.equipment.weapon;

import goldrush.equipment.WeaponEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class iKipper_Sword extends WeaponEquipment {
	private static ItemStack i;
	static {
		i = new ItemStack(Material.DIAMOND_SWORD);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.RED + "iKipper's Sword");
		i.setItemMeta(im);
	}

	public iKipper_Sword() {
		super(i, "iKipper's Sword");
		this.setSlot(23);
		this.setPrice(100);
	}
}