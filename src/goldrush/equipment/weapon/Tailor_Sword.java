package goldrush.equipment.weapon;

import goldrush.equipment.WeaponEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Tailor_Sword extends WeaponEquipment {
	private static ItemStack i;
	static {
		i = new ItemStack(Material.IRON_SWORD);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.RED + "Tailor's Sword");
		i.setItemMeta(im);
	}

	public Tailor_Sword() {
		super(i, "Tailor's Sword");
		this.setSlot(22);
		this.setPrice(50);
	}
}