package goldrush.equipment.weapon;

import goldrush.equipment.WeaponEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Mr_Nut_Sword extends WeaponEquipment {
	private static ItemStack i;
	static {
		i = new ItemStack(Material.STONE_SWORD);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.RED + "Mr_Nut's Sword");
		i.setItemMeta(im);
	}

	public Mr_Nut_Sword() {
		super(i, "Mr_Nut's Sword");
		this.setSlot(21);
		this.setPrice(25);
	}
}