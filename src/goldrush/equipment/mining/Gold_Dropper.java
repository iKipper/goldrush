package goldrush.equipment.mining;

import goldrush.equipment.MineEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Gold_Dropper extends MineEquipment {
	private static ItemStack i;

	static {
		i = new ItemStack(Material.STONE_PICKAXE);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.GOLD + "Gold Dropper");
		i.setItemMeta(im);
	}

	public Gold_Dropper() {
		super(i, "Gold Dropper");
		this.setMinGold(1);
		this.setMaxGold(3);
		this.setSlot(1);
		this.setPrice(40);
	}
}