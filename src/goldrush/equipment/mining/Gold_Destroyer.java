package goldrush.equipment.mining;

import goldrush.equipment.MineEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Gold_Destroyer extends MineEquipment {
	private static ItemStack i;

	static {
		i = new ItemStack(Material.IRON_PICKAXE);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.GOLD + "Gold Destroyer");
		i.setItemMeta(im);
	}

	public Gold_Destroyer() {
		super(i, "Gold Destroyer");
		this.setMinGold(1);
		this.setMaxGold(3);
		this.setSlot(2);
		this.setPrice(50);
	}
}