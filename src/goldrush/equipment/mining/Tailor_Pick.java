package goldrush.equipment.mining;

import goldrush.equipment.MineEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Tailor_Pick extends MineEquipment {
	private static ItemStack i;

	static {
		i = new ItemStack(Material.IRON_PICKAXE);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.RED + "Tailor's Pickaxe");
		i.setItemMeta(im);
	}

	public Tailor_Pick() {
		super(i, "Tailor's Pickaxe");
		this.setMinGold(1);
		this.setMaxGold(5);
		this.setSlot(7);
		this.setPrice(100);
	}
}