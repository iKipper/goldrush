package goldrush.equipment.mining;

import goldrush.equipment.MineEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class iKipper_Pick extends MineEquipment {
	private static ItemStack i;

	static {
		i = new ItemStack(Material.DIAMOND_PICKAXE);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.RED + "iKipper's Pickaxe");
		i.setItemMeta(im);
	}

	public iKipper_Pick() {
		super(i, "iKipper's Pickaxe");
		this.setMinGold(1);
		this.setMaxGold(8);
		this.setSlot(8);
		this.setPrice(250);
	}
}