package goldrush.equipment.mining;

import goldrush.equipment.MineEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Mr_Nut_Pick extends MineEquipment {
	private static ItemStack i;

	static {
		i = new ItemStack(Material.IRON_PICKAXE);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.RED + "Mr_Nut's Pickaxe");
		i.setItemMeta(im);
	}

	public Mr_Nut_Pick() {
		super(i, "Mr_Nut's Pickaxe");
		this.setMinGold(1);
		this.setMaxGold(4);
		this.setSlot(6);
		this.setPrice(75);
	}
}