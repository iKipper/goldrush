package goldrush.equipment.mining;

import goldrush.equipment.MineEquipment;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Gold_Picker extends MineEquipment {
	private static ItemStack i;

	static {
		i = new ItemStack(Material.STONE_PICKAXE);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(ChatColor.GOLD + "Gold Picker");
		i.setItemMeta(im);
	}

	public Gold_Picker() {
		super(i, "Gold Picker");
		this.setMaxGold(2);
		this.setMinGold(1);
		this.setSlot(0);
		this.setPrice(15);
	}
}