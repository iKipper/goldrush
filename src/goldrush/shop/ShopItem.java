package goldrush.shop;

import goldrush.GoldRush;
import goldrush.equipment.Equipment;
import goldrush.equipment.MineEquipment;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ShopItem {

	private Equipment equipment;
	private ItemStack displayItem;

	public ShopItem(Equipment equip) {
		this.equipment = equip;
		updateDisplayItem();
	}

	public void updateDisplayItem() {
		if (equipment != null) {
			this.displayItem = new ItemStack(equipment.getItem().getType());
			ItemMeta im = this.displayItem.getItemMeta();

			String name = equipment.getItem().getItemMeta().getDisplayName();

			int price = equipment.getPrice();
			
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.DARK_GRAY + "Price: " + ChatColor.GRAY + price + " Gold");
			
			if(equipment instanceof MineEquipment){
				MineEquipment me = (MineEquipment) equipment;
				int minG = me.getMinGold();
				int maxG = me.getMaxGold();
				lore.add("");
				lore.add(ChatColor.WHITE + "Between " + minG + " and " + maxG + " Gold");
			}
			
			im.setDisplayName(name);
			im.setLore(lore);

			this.displayItem.setItemMeta(im);
		} else {
			throw new NullPointerException("equipment is null");
		}
	}

	public ItemStack getDisplayItem() {
		return displayItem;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public int getPrice() {
		return equipment.getPrice();
	}

	/**
	 * Returns true based on the balance of a certain player. If a player has
	 * enough money to buy the equipment true if returned, else false is
	 * returned.
	 * 
	 * @param p
	 *            the player that is trying to buy the item
	 * @return
	 */
	public boolean playerCanBuy(Player p) {
		long bal = GoldRush.getInstance().getBalance().getPlayerBalance(p);
		if (bal >= getPrice())
			return true;
		return false;
	}

	public String getName() {
		return equipment.getName();
	}

	/**
	 * Returns true if the displayname if the two items matches. This should not
	 * create false positives if only GoldRush is run on the server, as there
	 * should never be more that one piece of equipment with a certain name, and
	 * thus only one shoptItem with that displayName.
	 * 
	 * @param item
	 *            the item being compared to the shopItem.
	 * @return
	 */
	public boolean displayItemMatches(ItemStack item) {
		if (item != null) {
			if (item.getItemMeta().getDisplayName().equals(this.displayItem.getItemMeta().getDisplayName())) {
				return true;
			}
		}
		return false;
	}

}
