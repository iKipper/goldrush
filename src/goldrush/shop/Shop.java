package goldrush.shop;

import goldrush.GoldRush;
import goldrush.equipment.Equipment;
import goldrush.equipment.EquipmentManager;
import goldrush.utils.IconMenu;
import goldrush.utils.IconMenu.OptionClickEvent;
import goldrush.utils.IconMenu.OptionClickEventHandler;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Shop implements OptionClickEventHandler {

	private IconMenu menu;
	private List<ShopItem> items;

	public Shop(EquipmentManager eManager) {
		menu = new IconMenu("Shop", 27, this, GoldRush.getInstance());
		loadItems(eManager);
	}

	private void loadItems(EquipmentManager eManager) {
		items = new ArrayList<ShopItem>();
		for (Equipment equip : eManager.getEquipment()) {
			ShopItem item = new ShopItem(equip);
			items.add(item);
			menu.setOption(equip.getSlot(), item);
		}
	}

	public void open(Player p) {
		menu.open(p);
	}

	@Override
	public void onOptionClick(OptionClickEvent event) {
		ItemStack item = event.getClickedItem();
		ShopItem sItem = getShopItem(item);
		if (sItem != null) {
			Player p = event.getPlayer();
			if (sItem.playerCanBuy(p)) {
				p.getInventory().addItem(sItem.getEquipment().getItem());
				GoldRush.getInstance().getBalance().getPlayerBalances()
						.put(event.getPlayer().getName(), GoldRush.getInstance().getBalance().getPlayerBalances().get(event.getPlayer().getName()) - sItem.getPrice());
				GoldRush.getInstance()
						.getGame()
						.initPlayerScoreboard(event.getPlayer(), GoldRush.getInstance().getBalance().getRedBalanace(), GoldRush.getInstance().getBalance().getBlueBalance(),
								GoldRush.getInstance().getGame().getRedCoreHealth(), GoldRush.getInstance().getGame().getBlueCoreHealth(),
								GoldRush.getInstance().getBalance().getPlayerBalances().get(event.getPlayer().getName()));
			} else {
				p.sendMessage(GoldRush.getInstance().getPrefix() + "You need more gold to purchase this.");
			}
		}
	}

	public ShopItem getShopItem(ItemStack item) {
		for (ShopItem i : items) {
			if (i.displayItemMatches(item)) {
				return i;
			}
		}
		return null;
	}

	public ItemStack getShopIcon() {
		ItemStack is = new ItemStack(Material.GOLD_INGOT);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.GOLD + "GoldRush Shop" + ChatColor.GRAY + " (Right-Click");
		is.setItemMeta(im);
		return is;
	}
}