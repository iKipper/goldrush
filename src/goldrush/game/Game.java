package goldrush.game;

import goldrush.GoldRush;
import goldrush.timers.LobbyTimer;
import goldrush.utils.IconMenu;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

public class Game {

	private GoldRush instance;
	private Map map;

	private Scoreboard board;
	private ArrayList<Player> teamRed;
	private ArrayList<Player> teamBlue;
	private Score redBalance;
	private Score blueBalance;
	private Score redCoreHealth;
	private Score blueCoreHealth;
	private Score playerBalance;

	private int redCoreHealthNum;
	private int blueCoreHealthNum;
	private List<Player> dead;
	private ArrayList<Player> protection;
	public IconMenu menu;

	public Game(GoldRush instance) {
		this.instance = instance;
		this.map = Map.random();
		this.teamRed = new ArrayList<Player>();
		this.teamBlue = new ArrayList<Player>();
		this.dead = new ArrayList<Player>();
		this.protection = new ArrayList<Player>();
	}

	/**
	 * Setup the Gold Rush game scoreboard.
	 */
	private void setupScoreboard(long redBalan, long blueBalan, int redCoreHe, int blueCoreHe, long playerBalan) {
		board = Bukkit.getScoreboardManager().getNewScoreboard();
		if (board.getObjective("mmm") == null)
			board.registerNewObjective("mmm", "mmm").setDisplaySlot(DisplaySlot.SIDEBAR);
		board.getObjective("mmm").setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Gold Rush");

		Score redBal = board.getObjective("mmm").getScore(ChatColor.RED + "Gold Balance");
		Score blueBal = board.getObjective("mmm").getScore(ChatColor.BLUE + "Gold Balance");
		Score redCore = board.getObjective("mmm").getScore(ChatColor.RED + "Core Health");
		Score blueCore = board.getObjective("mmm").getScore(ChatColor.BLUE + "Core Health");
		Score playerBal = board.getObjective("mmm").getScore(ChatColor.GOLD + "Your Balanace");

		Score red = board.getObjective("mmm").getScore(ChatColor.RED + "Red Team");
		Score blue = board.getObjective("mmm").getScore(ChatColor.BLUE + "Blue Team");
		Score empty = board.getObjective("mmm").getScore(" ");
		Score empty1 = board.getObjective("mmm").getScore(ChatColor.STRIKETHROUGH + "--------------");
		Score empty2 = board.getObjective("mmm").getScore("  ");

		redBalance = board.getObjective("mmm").getScore(ChatColor.RED + "  " + ChatColor.GREEN + redBalan);
		blueBalance = board.getObjective("mmm").getScore(ChatColor.BLUE + " Gold" + ChatColor.GREEN + blueBalan);
		redCoreHealth = board.getObjective("mmm").getScore(ChatColor.RED + "  " + ChatColor.GREEN + redCoreHe);
		blueCoreHealth = board.getObjective("mmm").getScore(ChatColor.BLUE + " Core" + ChatColor.GREEN + blueCoreHe);
		playerBalance = board.getObjective("mmm").getScore(ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.UNDERLINE + playerBalan);

		empty.setScore(14);
		red.setScore(13);
		redBal.setScore(12);
		redBalance.setScore(11);
		redCore.setScore(10);
		redCoreHealth.setScore(9);
		empty1.setScore(8);
		blue.setScore(7);
		blueBal.setScore(6);
		blueBalance.setScore(5);
		blueCore.setScore(4);
		blueCoreHealth.setScore(3);
		empty2.setScore(2);
		playerBal.setScore(1);
		playerBalance.setScore(0);
	}

	/**
	 * Set the target players scoreboard to the Gold Rush game scoreboard.
	 * 
	 * @param p
	 *            target player
	 * @param redBal
	 *            red new balance
	 * @param blueBal
	 *            blue new balance
	 * @param redCore
	 *            blue core new health
	 * @param blueCore
	 *            red core new health
	 */
	public void initPlayerScoreboard(Player p, long redBal, long blueBal, int redCore, int blueCore, long playerBal) {
		setupScoreboard(redBal, blueBal, redCore, blueCore, playerBal);
		redCoreHealthNum = redCore;
		blueCoreHealthNum = blueCore;
		p.setScoreboard(board);
	}

	/**
	 * Set the target player to a team based on size of the team.
	 * 
	 * @param p
	 *            target player
	 */
	public void initPlayerToTeams(Player p) {
		if (teamRed.size() > teamBlue.size()) {
			teamBlue.add(p);
			p.sendMessage(ChatColor.BLUE + "You are on the Blue team.");
		} else {
			teamRed.add(p);
			p.sendMessage(ChatColor.RED + "You are on the Red team.");
		}

		setPlayerListName(p);
	}

	/**
	 * Teleport the player to his teams spawn.
	 * 
	 * @param p
	 *            target player
	 */
	public void initPlayerToSpawn(Player p) {
		if (teamRed.contains(p)) {
			p.teleport(map.getRedTeamSpawn());
		} else {
			p.teleport(map.getBlueTeamSpawn());
		}
	}

	/**
	 * Initialized when the target player dies.
	 * 
	 * @param p
	 *            target player
	 */
	public void initPlayerDeath(final Player p) {
		if (p.getKiller() != null) {
			Bukkit.broadcastMessage(instance.getPrefix() + p.getName() + " was killed by " + p.getKiller().getName());
		}

		System.out.println(teamRed.size() + " | Red size");
		System.out.println(teamBlue.size() + " | Blue size");
		System.out.println(blueCoreHealthNum + "| Blue health");
		System.out.println(redCoreHealthNum + " | Red health");

		final ItemStack[] items = p.getInventory().getContents();
		final ItemStack[] inv = p.getInventory().getArmorContents();
		long halfBalance = instance.getBalance().getPlayerBalance(p) / 2;
		if (p.getKiller() != null) {
			long kilBal = instance.getBalance().getPlayerBalance(p.getKiller()) + halfBalance;
			instance.getBalance().getPlayerBalances().put(p.getKiller().getName(), kilBal);
		}
		instance.getBalance().getPlayerBalances().put(p.getName(), halfBalance);
		System.out.print(instance.getBalance().getPlayerBalance(p));
		initPlayerScoreboard(p, instance.getBalance().getRedBalanace(), instance.getBalance().getBlueBalance(), redCoreHealthNum, blueCoreHealthNum, instance.getBalance().getPlayerBalance(p));

		if (teamBlue.contains(p)) {
			if (blueCoreHealthNum == 0) {
				dead.add(p);
				teamBlue.remove(p);
				p.setAllowFlight(true);
				p.setFlying(true);
				p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
				p.sendMessage(instance.getPrefix() + "You core has been destroyed.");
				p.sendMessage(instance.getPrefix() + "You can not respawn.");

				if (LobbyTimer.isFinished()) {
					if (teamBlue.size() == 0) {
						initTeamWon(teamRed);
					}
				}
			} else {
				p.teleport(map.getBlueTeamSpawn());
				Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
					public void run() {
						p.getInventory().setContents(items);
						p.getInventory().setArmorContents(inv);

						if (!p.getInventory().contains(Material.STONE_SWORD)) {
							p.getInventory().addItem(new ItemStack(Material.STONE_SWORD));
						}

						if (!p.getInventory().contains(Material.STONE_PICKAXE)) {
							p.getInventory().addItem(new ItemStack(Material.STONE_PICKAXE));
						}
					}
				}, 3);
			}
		} else if (teamRed.contains(p)) {
			if (redCoreHealthNum == 0) {
				dead.add(p);
				teamRed.remove(p);
				p.setAllowFlight(true);
				p.setFlying(true);
				p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
				p.sendMessage(instance.getPrefix() + "You core has been destroyed.");
				p.sendMessage(instance.getPrefix() + "You can not respawn.");

				if (LobbyTimer.isFinished()) {
					if (teamRed.size() == 0) {
						initTeamWon(teamBlue);
					}
				}
			} else {
				p.teleport(map.getRedTeamSpawn());
				Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
					public void run() {
						p.getInventory().setContents(items);
						p.getInventory().setArmorContents(inv);

						if (!p.getInventory().contains(Material.STONE_SWORD)) {
							p.getInventory().addItem(new ItemStack(Material.STONE_SWORD));
						}

						if (!p.getInventory().contains(Material.STONE_PICKAXE)) {
							p.getInventory().addItem(new ItemStack(Material.STONE_PICKAXE));
						}
					}
				}, 3);
			}
		}

		protection.add(p);
		Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
			public void run() {
				protection.remove(p);
			}
		}, 60);
	}

	/**
	 * Initialize the target players inventory items.
	 * 
	 * @param p
	 *            target player
	 */
	public void initPlayerItems(final Player p) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
			public void run() {
				p.getInventory().addItem(new ItemStack(Material.WOOD_SWORD));
				p.getInventory().addItem(new ItemStack(Material.STONE_PICKAXE));
				p.getInventory().setItem(8, instance.getShop().getShopIcon());
				p.getInventory().setHelmet(new ItemStack(Material.GOLD_HELMET));
				p.getInventory().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
				p.getInventory().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
				p.getInventory().setBoots(new ItemStack(Material.GOLD_BOOTS));
			}
		}, 5L);
	}

	/**
	 * Initialize the winning target team.
	 * 
	 * @param team
	 *            target team
	 */
	public void initTeamWon(ArrayList<Player> team) {
		if (team == teamBlue) {
			Bukkit.broadcastMessage(ChatColor.BLUE + "" + ChatColor.MAGIC + "&&&&&&&&&&&&&&&&&&&&&");
			Bukkit.broadcastMessage(ChatColor.BLUE + "" + ChatColor.MAGIC + "& " + ChatColor.RESET + "" + ChatColor.BLUE + "  Blue team has won! " + ChatColor.MAGIC + " &");
			Bukkit.broadcastMessage(ChatColor.BLUE + "" + ChatColor.MAGIC + "&&&&&&&&&&&&&&&&&&&&&");
		} else if (team == teamRed) {
			Bukkit.broadcastMessage(ChatColor.RED + "" + ChatColor.MAGIC + "&&&&&&&&&&&&&&&&&&&&&");
			Bukkit.broadcastMessage(ChatColor.RED + "" + ChatColor.MAGIC + "& " + ChatColor.RESET + "" + ChatColor.RED + "  Red team has won! " + ChatColor.MAGIC + " &");
			Bukkit.broadcastMessage(ChatColor.RED + "" + ChatColor.MAGIC + "&&&&&&&&&&&&&&&&&&&&&");
		}

		for (final Player p : Bukkit.getOnlinePlayers()) {
			freshStart(p);

			Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
				public void run() {
					instance.sendPlayer(p, "hub");
				}
			}, 80L);
		}

		Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
			public void run() {
				System.exit(0);
			}
		}, 100L);
	}

	/**
	 * Clears the players inventory and heals them while removing potion
	 * effects.
	 * 
	 * @param p
	 *            target player
	 */
	public void freshStart(Player p) {
		p.setHealth(20.0);
		p.setFoodLevel(20);
		p.setSaturation(20.0f);
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		p.setFireTicks(0);
		p.setLevel(0);
		p.setExp(0.0f);
		p.setGameMode(GameMode.SURVIVAL);
		p.setItemOnCursor(null);
		for (PotionEffect pe : p.getActivePotionEffects()) {
			p.removePotionEffect(pe.getType());
		}
	}

	public ItemStack getGameBook() {
		ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
		BookMeta meta = (BookMeta) book.getItemMeta();
		meta.addPage("Hello, this guide will help you to understand of the game GoldRush works. So you might be asking, what's the objective? Well, here is how you play. Firstly, you will be put in to a team, you have to work together with your team to win the game!");
		meta.addPage("There is two ways to win, number one being destroy the other teams core and kill them all and number two being deposit 2,000 gold in to your teams balance, both works. More coming soon...");
		meta.setTitle(ChatColor.GOLD + "GoldRush");
		meta.setAuthor(ChatColor.RED + "iKipper");
		book.setItemMeta(meta);
		return book;
	}

	private void setPlayerListName(Player p) {
		if (teamRed.contains(p)) {
			String red = ChatColor.RED + p.getName();
			if (red.length() > 16)
				red = red.substring(0, 15);
			p.setPlayerListName(red);
		} else if (teamBlue.contains(p)) {
			String blue = ChatColor.BLUE + p.getName();
			if (blue.length() > 16)
				blue = blue.substring(0, 15);
			p.setPlayerListName(blue);
		}
	}

	/**
	 * 
	 * @param p
	 *            target player
	 * @return target player dead
	 */
	public boolean isDead(Player p) {
		if (dead.contains(p))
			return true;
		return false;
	}

	/**
	 * 
	 * @return red team
	 */
	public ArrayList<Player> getRedTeam() {
		return teamRed;
	}

	/**
	 * 
	 * @return blue team
	 */
	public ArrayList<Player> getBlueTeam() {
		return teamBlue;
	}

	/**
	 * 
	 * @return red balance score
	 */
	public Score getRedBalance() {
		return redBalance;
	}

	/**
	 * 
	 * @return blue balance score
	 */
	public Score getBlueBalance() {
		return blueBalance;
	}

	/**
	 * 
	 * @return red cores health score
	 */
	public Score getRedCoreHealthScore() {
		return redCoreHealth;
	}

	/**
	 * 
	 * @return blue cores health score
	 */
	public Score getBlueCoreHealthScore() {
		return blueCoreHealth;
	}

	/**
	 * 
	 * @return red core health
	 */
	public int getRedCoreHealth() {
		return redCoreHealthNum;
	}

	/**
	 * 
	 * @return blue core health
	 */
	public int getBlueCoreHealth() {
		return blueCoreHealthNum;
	}

	/**
	 * 
	 * @return map class
	 */
	public Map getMap() {
		return map;
	}

	/**
	 * 
	 * @return list of dead players
	 */
	public List<Player> getDeadPlayers() {
		return dead;
	}

	/**
	 * 
	 * @return list of protected players
	 */
	public ArrayList<Player> getSpawnProtection() {
		return protection;
	}

	/**
	 * Sets the red teams core health.
	 * 
	 * @param amount
	 *            new red core health
	 */
	public void setRedCoreHealth(int amount) {
		redCoreHealthNum = amount;
	}

	/**
	 * Sets the blue teams core health.
	 * 
	 * @param amount
	 *            new blue core health
	 */
	public void setBlueCoreHealth(int amount) {
		blueCoreHealthNum = amount;
	}
}