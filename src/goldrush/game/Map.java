package goldrush.game;

import goldrush.GoldRush;

import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import com.gmail.filoghost.holograms.api.HolographicDisplaysAPI;

public class Map {

	private GoldRush instance;

	private String mapName;
	private Location redSpawn;
	private Location blueSpawn;
	private Location redCore;
	private Location blueCore;
	private Location lobby;
	private Location redBank;
	private Location blueBank;
	private Mine mine;

	private Map(GoldRush instance, String mapName) {
		this.mapName = mapName;
		this.instance = instance;
		this.lobby = new Location(Bukkit.getWorld("world"), 0, 200, 0);
	}

	public void initializeMap(String mapName) {
		unloadWorld(mapName);
		instance.getRollback().rollback(mapName);
		loadWorld(mapName);
		removeEntities(mapName);
		redSpawn = new Location(Bukkit.getWorld(mapName), instance.getConfig().getDouble(mapName + ".Red.Spawn.X"), instance.getConfig().getDouble(mapName + ".Red.Spawn.Y"), instance.getConfig()
				.getDouble(mapName + ".Red.Spawn.Z"));
		blueSpawn = new Location(Bukkit.getWorld(mapName), instance.getConfig().getDouble(mapName + ".Blue.Spawn.X"), instance.getConfig().getDouble(mapName + ".Blue.Spawn.Y"), instance.getConfig()
				.getDouble(mapName + ".Blue.Spawn.Z"));
		redCore = new Location(Bukkit.getWorld(mapName), instance.getConfig().getDouble(mapName + ".Red.Core.X"), instance.getConfig().getDouble(mapName + ".Red.Core.Y"), instance.getConfig()
				.getDouble(mapName + ".Red.Core.Z"));
		blueCore = new Location(Bukkit.getWorld(mapName), instance.getConfig().getDouble(mapName + ".Blue.Core.X"), instance.getConfig().getDouble(mapName + ".Blue.Core.Y"), instance.getConfig()
				.getDouble(mapName + ".Blue.Core.Z"));
		redBank = new Location(Bukkit.getWorld(mapName), instance.getConfig().getDouble(mapName + ".Red.Bank.X"), instance.getConfig().getDouble(mapName + ".Red.Bank.Y") + 2, instance.getConfig()
				.getDouble(mapName + ".Red.Bank.Z"));
		blueBank = new Location(Bukkit.getWorld(mapName), instance.getConfig().getDouble(mapName + ".Blue.Bank.X"), instance.getConfig().getDouble(mapName + ".Blue.Bank.Y") + 2, instance.getConfig()
				.getDouble(mapName + ".Blue.Bank.Z"));
		Location l1 = new Location(Bukkit.getWorld(mapName), instance.getConfig().getDouble(mapName + ".Mine.X1"), instance.getConfig().getDouble(mapName + ".Mine.Y1"), instance.getConfig()
				.getDouble(mapName + ".Mine.Z1"));
		Location l2 = new Location(Bukkit.getWorld(mapName), instance.getConfig().getDouble(mapName + ".Mine.X2"), instance.getConfig().getDouble(mapName + ".Mine.Y2"), instance.getConfig()
				.getDouble(mapName + ".Mine.Z2"));
		mine = new Mine(l1, l2);

		Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
			public void run() {
				HolographicDisplaysAPI.createHologram(instance, redBank, ChatColor.GOLD + "" + ChatColor.BOLD + "Red Bank " + ChatColor.GRAY + "(Right-Click)");
				HolographicDisplaysAPI.createHologram(instance, blueBank, ChatColor.GOLD + "" + ChatColor.BOLD + "Blue Bank " + ChatColor.GRAY + "(Right-Click)");
			}
		}, 100L);
	}

	/**
	 * Chooses a random map from the map list.
	 */
	public static Map random() {
		GoldRush gr = GoldRush.getInstance();
		List<String> maps = gr.getConfig().getStringList("Maps");
		Random rand = new Random();
		int i = rand.nextInt(maps.size());
		String name = maps.get(i);
		return new Map(gr, name);
	}

	/**
	 * Unloads the target world.
	 * 
	 * @param world
	 *            the target world
	 */
	public void unloadWorld(String world) {
		if (Bukkit.getWorlds().contains(Bukkit.getWorld(world))) {
			for (Player p : Bukkit.getWorld(mapName).getPlayers()) {
				p.teleport(getLobbyLocation());
			}

			if (Bukkit.getServer().unloadWorld(Bukkit.getServer().getWorld(world), false)) {
				System.out.println("Map unloaded: " + world);
			} else {
				System.out.println("Map couldn't unload");
			}
		}
	}

	/**
	 * Loads the target world.
	 * 
	 * @param world
	 *            the target world
	 */
	public void loadWorld(String world) {
		GoldRush.getInstance().getRollback().rollback(mapName);
		World w = Bukkit.getServer().createWorld(new WorldCreator(world));
		w.setAutoSave(false);
		w.setWeatherDuration(0);
		w.setTime(0);
		w.setThundering(false);
		System.out.println(w.getName() + " loaded");
	}

	/**
	 * Removes all entities for the target world.
	 * 
	 * @param world
	 *            the target world
	 */
	public void removeEntities(String world) {
		for (Entity en : Bukkit.getWorld(world).getEntities()) {
			if (en.getType() != EntityType.PLAYER || en.getType() != EntityType.ITEM_FRAME) {
				en.remove();
			}
		}
	}

	/**
	 * 
	 * @return blue teams spawn
	 */
	public Location getRedTeamSpawn() {
		return redSpawn;
	}

	/**
	 * 
	 * @return red teams spawn
	 */
	public Location getBlueTeamSpawn() {
		return blueSpawn;
	}

	/**
	 * 
	 * @return game lobby location
	 */
	public Location getLobbyLocation() {
		return lobby;
	}

	/**
	 * 
	 * @return blue teams core location
	 */
	public Location getRedCore() {
		return redCore;
	}

	/**
	 * 
	 * @return red teams core location
	 */
	public Location getBlueCore() {
		return blueCore;
	}

	/**
	 * 
	 * @return red teams bank location
	 */
	public Location getRedBank() {
		return redBank;
	}

	/**
	 * 
	 * @return blue teams bank location
	 */
	public Location getBlueBank() {
		return blueBank;
	}

	/**
	 * 
	 * @return maps mines
	 */
	public Mine getMine() {
		return mine;
	}

	/**
	 * 
	 * @return current map name
	 */
	public String getMapName() {
		return mapName;
	}
}