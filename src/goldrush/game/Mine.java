package goldrush.game;

import org.bukkit.Location;
import org.bukkit.block.Block;

public class Mine {

	private Location corner1;
	private Location corner2;

	public Mine(Location l1, Location l2) {
		corner1 = l1;
		corner2 = l2;
	}

	public boolean isLocationInside(Location l) {
		int lowX = corner1.getBlockX() < corner2.getBlockX() ? corner1.getBlockX() : corner2.getBlockX();
		int lowY = corner1.getBlockY() < corner2.getBlockY() ? corner1.getBlockY() : corner2.getBlockY();
		int lowZ = corner1.getBlockZ() < corner2.getBlockZ() ? corner1.getBlockZ() : corner2.getBlockZ();

		int highX = corner1.getBlockX() > corner2.getBlockX() ? corner1.getBlockX() : corner2.getBlockX();
		int highY = corner1.getBlockY() > corner2.getBlockY() ? corner1.getBlockY() : corner2.getBlockY();
		int highZ = corner1.getBlockZ() > corner2.getBlockZ() ? corner1.getBlockZ() : corner2.getBlockZ();

		if ((l.getBlockX() >= lowX && l.getBlockX() <= highX) && (l.getBlockY() >= lowY && l.getBlockY() <= highY) && (l.getBlockZ() >= lowZ && l.getBlockZ() <= highZ)) {
			return true;
		}

		return false;
	}

	public boolean isBlockInside(Block bl) {
		return isLocationInside(bl.getLocation());
	}
}