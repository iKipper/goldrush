package goldrush.listeners;

import goldrush.GoldRush;
import goldrush.timers.LobbyTimer;
import goldrush.utils.IconMenu;
import goldrush.utils.IconMenu.OptionClickEvent;
import me.confuser.barapi.BarAPI;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.kitteh.tag.AsyncPlayerReceiveNameTagEvent;

public class PlayerListener implements Listener {

	private GoldRush instance;

	public PlayerListener(GoldRush instance) {
		this.instance = instance;
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		e.setJoinMessage(ChatColor.GRAY + e.getPlayer().getName() + ChatColor.DARK_GRAY + " has joined the game.");

		instance.getGame().freshStart(e.getPlayer());

		e.getPlayer().teleport(new Location(Bukkit.getWorld("world"), 0, 200, 0));
		e.getPlayer().getInventory().setItem(4, instance.getGame().getGameBook());

		if (instance.getBalance().getPlayerBalances().get(e.getPlayer().getName()) == null)
			instance.getBalance().getPlayerBalances().put(e.getPlayer().getName(), 0L);

		if (!LobbyTimer.isFinished()) {
			if (!LobbyTimer.isRunning()) {
				int num = 4 - Bukkit.getOnlinePlayers().length;
				for (Player p : Bukkit.getOnlinePlayers())
					BarAPI.setMessage(p, ChatColor.GOLD + "GoldRush needs " + ChatColor.GREEN + num + ChatColor.GOLD + " more players to start.");
			}

			if (!LobbyTimer.isRunning() && LobbyTimer.isReady()) {
				LobbyTimer.start();
			}
		} else {

			if (!instance.getGame().getDeadPlayers().contains(e.getPlayer())) {
				System.out.println("initializing player as alive");
				instance.getGame().initPlayerToTeams(e.getPlayer());
				instance.getGame().initPlayerItems(e.getPlayer());
				instance.getGame().initPlayerToSpawn(e.getPlayer());
			} else {
				System.out.println("initializing player as dead");
				e.getPlayer().setAllowFlight(true);
				e.getPlayer().setFlying(true);
				e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
				e.getPlayer().sendMessage(instance.getPrefix() + "You logged out while dead.");
			}

			instance.getGame().initPlayerScoreboard(e.getPlayer(), instance.getBalance().getRedBalanace(), instance.getBalance().getBlueBalance(), instance.getGame().getRedCoreHealth(),
					instance.getGame().getBlueCoreHealth(), instance.getBalance().getPlayerBalance(e.getPlayer()));
		}
	}

	@EventHandler
	public void onPlayerQuit(final PlayerQuitEvent e) {
		e.setQuitMessage(ChatColor.GRAY + e.getPlayer().getName() + ChatColor.DARK_GRAY + " has quit the game.");

		Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
			public void run() {
				if (!LobbyTimer.isFinished()) {
					if (LobbyTimer.isRunning() && !LobbyTimer.isReady()) {
						LobbyTimer.setTimer(61);
						LobbyTimer.stop();
						Bukkit.broadcastMessage(instance.getPrefix() + "Not enough players. Timer stopped.");
					}
				} else {
					if (instance.getGame().getRedTeam().contains(e.getPlayer())) {
						instance.getGame().getRedTeam().remove(e.getPlayer());
					} else if (instance.getGame().getBlueTeam().contains(e.getPlayer())) {
						instance.getGame().getBlueTeam().remove(e.getPlayer());
					}
					
					if(instance.getGame().getRedTeam().size() == 0) {
						instance.getGame().initTeamWon(instance.getGame().getBlueTeam());
					} else if(instance.getGame().getBlueTeam().size() == 0) {
						instance.getGame().initTeamWon(instance.getGame().getRedTeam());
					}

					if (!LobbyTimer.isReady()) {
						for (Player p : Bukkit.getOnlinePlayers()) {
							instance.sendPlayer(p, "hub");
						}

						Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
							public void run() {
								System.exit(0);
							}
						}, 30);
					}
				}

				instance.getBalance().getPlayerBalances().remove(e.getPlayer().getName());
			}
		}, 3);
	}

	@EventHandler
	public void onPlayerPickupItem(final PlayerPickupItemEvent e) {
		if (LobbyTimer.isFinished()) {
			if (!instance.getGame().getDeadPlayers().contains(e.getPlayer())) {
				if (e.getItem().getItemStack().getType() == Material.GOLD_NUGGET) {
					int amount = e.getItem().getItemStack().getAmount();
					instance.getBalance().getPlayerBalances().put(e.getPlayer().getName(), instance.getBalance().getPlayerBalances().get(e.getPlayer().getName()) + amount);

					instance.getGame().initPlayerScoreboard(e.getPlayer(), instance.getBalance().getRedBalanace(), instance.getBalance().getBlueBalance(), instance.getGame().getRedCoreHealth(),
							instance.getGame().getBlueCoreHealth(), instance.getBalance().getPlayerBalances().get(e.getPlayer().getName()));

					Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
						public void run() {
							e.getPlayer().getInventory().remove(e.getItem().getItemStack().getType());
						}
					}, 1);
				}
			}
		}
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		e.setDeathMessage(null);
		e.getEntity().setHealth(20.0);
		instance.getGame().initPlayerDeath(e.getEntity());
		e.getDrops().clear();
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerInteract(final PlayerInteractEvent e) {
		if (!instance.getGame().getDeadPlayers().contains(e.getPlayer())) {
			if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (e.getPlayer().getItemInHand().equals(instance.getShop().getShopIcon())) {
					instance.getShop().open(e.getPlayer());
					return;
				}
			}
		}

		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (e.getClickedBlock().getType() == Material.ENDER_CHEST) {
				e.getPlayer().closeInventory();

				final long amount = instance.getBalance().getPlayerBalance(e.getPlayer());
				instance.getGame().menu = new IconMenu("Deposit all your gold?", 27, new IconMenu.OptionClickEventHandler() {
					@Override
					public void onOptionClick(OptionClickEvent event) {
						if (event.getPosition() == 11) {
							if (amount > 0) {
								if (instance.getGame().getRedTeam().contains(e.getPlayer())) {
									instance.getBalance().setRedBalance(instance.getBalance().getRedBalanace() + amount);
									instance.getBalance().getPlayerBalances().put(e.getPlayer().getName(), 0L);

									if (instance.getBalance().getRedBalanace() >= 2000) {
										instance.getGame().initTeamWon(instance.getGame().getRedTeam());
									}
								} else if (instance.getGame().getBlueTeam().contains(e.getPlayer())) {
									instance.getBalance().setBlueBalance(instance.getBalance().getBlueBalance() + amount);
									instance.getBalance().getPlayerBalances().put(e.getPlayer().getName(), 0L);

									if (instance.getBalance().getBlueBalance() >= 2000) {
										instance.getGame().initTeamWon(instance.getGame().getBlueTeam());
									}
								}

								for (Player p : Bukkit.getOnlinePlayers()) {
									instance.getGame().initPlayerScoreboard(p, instance.getBalance().getRedBalanace(), instance.getBalance().getBlueBalance(), instance.getGame().getRedCoreHealth(),
											instance.getGame().getBlueCoreHealth(), instance.getBalance().getPlayerBalances().get(p.getName()));
								}
							} else {
								event.setWillClose(true);
								event.setWillDestroy(true);
								e.getPlayer().sendMessage(instance.getPrefix() + "You don't have any nuggets to deposit.");
							}
						} else if (event.getPosition() == 15) {
							event.setWillClose(true);
							event.setWillDestroy(true);
						}
					}
				}, instance).setOption(11, new ItemStack(Material.STAINED_CLAY, 1, DyeColor.GREEN.getData()), ChatColor.GOLD + "Yes?", ChatColor.WHITE + "" + ChatColor.ITALIC + "Are you sure?")
						.setOption(15, new ItemStack(Material.STAINED_CLAY, 1, DyeColor.RED.getData()), ChatColor.GOLD + "No?", ChatColor.WHITE + "" + ChatColor.ITALIC + "Are you sure?");

				Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
					public void run() {
						instance.getGame().menu.open(e.getPlayer());
					}
				}, 3);
			}
		}
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		if (e.getInventory().getName().equalsIgnoreCase("Deposit all your gold?")) {
			instance.getGame().menu.destroy();
		}
	}

	@EventHandler
	public void onTagChange(AsyncPlayerReceiveNameTagEvent e) {
		if (instance.getGame().getBlueTeam().contains(e.getNamedPlayer())) {
			e.setTag(ChatColor.BLUE + e.getNamedPlayer().getName());
		} else if (instance.getGame().getRedTeam().contains(e.getNamedPlayer())) {
			e.setTag(ChatColor.RED + e.getNamedPlayer().getName());
		}
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e) {
		if (LobbyTimer.isFinished()) {
			if (instance.getGame().getRedTeam().contains(e.getPlayer())) {
				e.setFormat(ChatColor.RED + "[Red] " + e.getPlayer().getName() + ": " + ChatColor.GRAY + e.getMessage());
			} else if (instance.getGame().getBlueTeam().contains(e.getPlayer())) {
				e.setFormat(ChatColor.BLUE + "[Blue] " + e.getPlayer().getName() + ": " + ChatColor.GRAY + e.getMessage());
			} else if (instance.getGame().getDeadPlayers().contains(e.getPlayer())) {
				e.setFormat(ChatColor.GOLD + "[Dead] " + e.getPlayer().getName() + ": " + ChatColor.GRAY + e.getMessage());
			}
		} else {
			if (e.getPlayer().getName().equalsIgnoreCase("iKipper") || e.getPlayer().getName().equalsIgnoreCase("Sgt_Tailor")) {
				e.setFormat(ChatColor.GOLD + "[Dev] " + e.getPlayer().getName() + ": " + e.getMessage());
			} else {
				e.setFormat(ChatColor.GRAY + e.getPlayer().getName() + ": " + e.getMessage());
			}
		}
	}

	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent e) {
		if (!LobbyTimer.isFinished()) {
			e.setCancelled(true);
		}
	}
}