package goldrush.listeners;

import goldrush.GoldRush;
import goldrush.equipment.Equipment;
import goldrush.equipment.MineEquipment;
import goldrush.timers.LobbyTimer;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.ItemStack;

public class WorldListener implements Listener {

	private GoldRush instance;

	public WorldListener(GoldRush instance) {
		this.instance = instance;
	}

	@EventHandler
	public void onBlockBreak(final BlockBreakEvent e) {
		if (!LobbyTimer.isFinished()) {
			e.setCancelled(true);
			return;
		}

		if (instance.getGame().isDead(e.getPlayer())) {
			e.setCancelled(true);
			return;
		}

		if (!instance.getGame().getMap().getMine().isBlockInside(e.getBlock())) {
			if (!e.getBlock().getLocation().equals(instance.getGame().getMap().getRedCore()) && !e.getBlock().getLocation().equals(instance.getGame().getMap().getBlueCore())) {
				e.setCancelled(true);
				return;
			}
		}

		if (instance.getGame().getMap().getMine().isBlockInside(e.getBlock())) {
			if (e.getBlock().getType() == Material.GOLD_ORE) {
				ItemStack item = e.getPlayer().getItemInHand();
				Equipment equip = instance.getEquipmentManager().getEquipment(item);
				if (equip != null) {
					if (equip instanceof MineEquipment) {
						int amount = ((MineEquipment) equip).getRandomGold();
						e.getBlock().getDrops().clear();
						e.getBlock().getLocation().getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.GOLD_NUGGET, amount));
						instance.getGame().initPlayerScoreboard(e.getPlayer(), instance.getBalance().getRedBalanace(), instance.getBalance().getBlueBalance(), instance.getGame().getRedCoreHealth(), instance.getGame().getBlueCoreHealth(), instance.getBalance().getPlayerBalances().get(e.getPlayer().getName()));
					}
				} else {
					e.getBlock().getLocation().getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.GOLD_NUGGET, 1));
					instance.getGame().initPlayerScoreboard(e.getPlayer(), instance.getBalance().getRedBalanace(), instance.getBalance().getBlueBalance(), instance.getGame().getRedCoreHealth(), instance.getGame().getBlueCoreHealth(), instance.getBalance().getPlayerBalances().get(e.getPlayer().getName()));
				}
				final BlockState bs = e.getBlock().getState();
				Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable(){
					@SuppressWarnings("deprecation")
					public void run() {
						e.getBlock().setType(bs.getType());
						e.getBlock().setData(bs.getBlock().getData());
					}
				}, 20 * 60 * 2);
				e.getBlock().setType(Material.AIR);
			}
		}

		if (instance.getGame().getRedTeam().contains(e.getPlayer())) {
			if (e.getBlock().getLocation().equals(instance.getGame().getMap().getRedCore())) {
				e.getPlayer().sendMessage(instance.getPrefix() + "This is your core, defend it!");
				e.setCancelled(true);
				return;
			}

			if (e.getBlock().getLocation().equals(instance.getGame().getMap().getBlueCore())) {
				if (instance.getBalance().getRedBalanace() < 500) {
					e.getPlayer().sendMessage(instance.getPrefix() + "Your teams balance must be over 500!");
					e.setCancelled(true);
					return;
				}

				if (instance.getGame().getBlueCoreHealth() == 0) {
					Bukkit.broadcastMessage(instance.getPrefix() + "Blue teams core has been destroyed!");
					Bukkit.broadcastMessage(instance.getPrefix() + "Blue team can no longer respawn!");
					e.getBlock().setType(Material.AIR);
				} else {
					instance.getGame().setBlueCoreHealth(instance.getGame().getBlueCoreHealth() - 1);
					Bukkit.broadcastMessage(instance.getPrefix() + "Blues core is under attack!");
					e.setCancelled(true);
				}
				for (Player p : Bukkit.getOnlinePlayers()) {
					instance.getGame().initPlayerScoreboard(p, instance.getBalance().getRedBalanace(), instance.getBalance().getBlueBalance(), instance.getGame().getRedCoreHealth(), instance.getGame().getBlueCoreHealth(), instance.getBalance().getPlayerBalances().get(p.getName()));
				}
			}
		} else if (instance.getGame().getBlueTeam().contains(e.getPlayer())) {
			if (e.getBlock().getLocation().equals(instance.getGame().getMap().getBlueCore())) {
				e.getPlayer().sendMessage(instance.getPrefix() + "This is your core, defend it!");
				e.setCancelled(true);
				return;
			}

			if (e.getBlock().getLocation().equals(instance.getGame().getMap().getRedCore())) {
				if (instance.getBalance().getBlueBalance() < 500) {
					e.getPlayer().sendMessage(instance.getPrefix() + "Your teams balance must be over 500!");
					e.setCancelled(true);
					return;
				}

				if (instance.getGame().getRedCoreHealth() == 0) {
					Bukkit.broadcastMessage(instance.getPrefix() + "Red teams core has been destroyed!");
					Bukkit.broadcastMessage(instance.getPrefix() + "Red team can no longer respawn!");
					e.getBlock().setType(Material.AIR);
				} else {
					instance.getGame().setRedCoreHealth(instance.getGame().getRedCoreHealth() - 1);
					Bukkit.broadcastMessage(instance.getPrefix() + "Reds core is under attack!");
					e.setCancelled(true);
				}
				for (Player p : Bukkit.getOnlinePlayers()) {
					instance.getGame().initPlayerScoreboard(p, instance.getBalance().getRedBalanace(), instance.getBalance().getBlueBalance(), instance.getGame().getRedCoreHealth(), instance.getGame().getBlueCoreHealth(), instance.getBalance().getPlayerBalances().get(p.getName()));
				}
			}
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {
		if (!LobbyTimer.isFinished()) {
			e.setCancelled(true);
			return;
		}

		if (!instance.getGame().getMap().getMine().isBlockInside(e.getBlock())) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onWeatherChange(WeatherChangeEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		if (!LobbyTimer.isFinished()) {
			e.setCancelled(true);
			return;
		}

		if (e.getEntity().getType() == EntityType.PLAYER && e.getDamager().getType() == EntityType.PLAYER) {
			Player p = (Player) e.getEntity();
			Player k = (Player) e.getDamager();

			if (instance.getGame().getRedTeam().contains(p) && instance.getGame().getRedTeam().contains(k)) {
				e.setCancelled(true);
			} else if (instance.getGame().getBlueTeam().contains(p) && instance.getGame().getBlueTeam().contains(k)) {
				e.setCancelled(true);
			}
			
			if (instance.getGame().getSpawnProtection().contains(p)) {
				k.sendMessage(instance.getPrefix() + "That player has spawn protection.");
				e.setCancelled(true);
			}
			
			if(instance.getGame().getDeadPlayers().contains(k)) {
				k.sendMessage(instance.getPrefix() + "You are dead.");
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent e) {
		if(instance.getGame().getSpawnProtection().contains((Player) e.getEntity())) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent e) {
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onItemDamage(PlayerItemBreakEvent e) {
		ItemStack item = e.getBrokenItem();
		e.getPlayer().getInventory().addItem(item);
	}
}