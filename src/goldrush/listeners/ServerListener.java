package goldrush.listeners;

import goldrush.GoldRush;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class ServerListener implements Listener {
	
	private GoldRush instance;
	
	public ServerListener(GoldRush instance) {
		this.instance = instance;
	}

	@EventHandler
	public void onServerListPing(ServerListPingEvent e) {
		e.setMotd(instance.getGame().getMap().getMapName());
	}
}
