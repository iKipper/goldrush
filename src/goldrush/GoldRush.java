package goldrush;

import goldrush.apis.BalanceApi;
import goldrush.apis.DatabaseApi;
import goldrush.equipment.EquipmentManager;
import goldrush.equipment.armor.DiamondBoots;
import goldrush.equipment.armor.DiamondChestplate;
import goldrush.equipment.armor.DiamondHelmet;
import goldrush.equipment.armor.DiamondLeggings;
import goldrush.equipment.armor.IronBoots;
import goldrush.equipment.armor.IronChestplate;
import goldrush.equipment.armor.IronHelmet;
import goldrush.equipment.armor.IronLeggings;
import goldrush.equipment.mining.Gold_Destroyer;
import goldrush.equipment.mining.Gold_Dropper;
import goldrush.equipment.mining.Gold_Picker;
import goldrush.equipment.mining.Mr_Nut_Pick;
import goldrush.equipment.mining.Tailor_Pick;
import goldrush.equipment.mining.iKipper_Pick;
import goldrush.equipment.weapon.Tailor_Sword;
import goldrush.equipment.weapon.iKipper_Sword;
import goldrush.game.Game;
import goldrush.listeners.PlayerListener;
import goldrush.listeners.ServerListener;
import goldrush.listeners.WorldListener;
import goldrush.shop.Shop;
import goldrush.timers.LobbyTimer;
import goldrush.utils.Rollback;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.filoghost.holograms.api.HolographicDisplaysAPI;

public class GoldRush extends JavaPlugin {

	private Game game;
	private DatabaseApi dApi;
	private Rollback rollback;
	private LobbyTimer timer;
	private BalanceApi balance;
	private static GoldRush instance;
	private EquipmentManager equipmentManager;
	private Shop shop;

	public void onEnable() {
		instance = this;

		for (Player p : Bukkit.getOnlinePlayers()) {
			sendPlayer(p, "hub");
		}
		setRollback(new Rollback(this));
		setGame(new Game(this));
		setdApi(new DatabaseApi());
		setTimer(new LobbyTimer(this));
		setBalance(new BalanceApi());

		saveDefaultConfig();

		loadEquipments();
		shop = new Shop(equipmentManager);
		Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);
		Bukkit.getPluginManager().registerEvents(new WorldListener(this), this);
		Bukkit.getPluginManager().registerEvents(new ServerListener(this), this);

		instance.getGame().getMap().initializeMap(instance.getGame().getMap().getMapName());
		System.out.println(Bukkit.getWorlds().toString());

		HolographicDisplaysAPI.createHologram(this, new Location(Bukkit.getWorld("world"), 0.5, 203, 0.5), ChatColor.RED + "Game by: " + ChatColor.WHITE + "iKipper, Sgt_Tailor");
	
		System.out.println("Testing new file upload?");
	}

	public void onDisable() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			sendPlayer(p, "hub");
		}
	}

	/**
	 * Returns the shop used to buy equipment
	 * 
	 * @return a shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * 
	 * @return GoldRush main class
	 */
	public static GoldRush getInstance() {
		return instance;
	}

	/**
	 * 
	 * @return GoldRush game prefix
	 */
	public String getPrefix() {
		return ChatColor.GOLD + "[GoldRush] " + ChatColor.AQUA;
	}

	/**
	 * 
	 * @return GoldRush game class
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * 
	 * @return GoldRush database class
	 */
	public DatabaseApi getDatabaseApi() {
		return dApi;
	}

	/**
	 * 
	 * @return GoldRush rollback class
	 */
	public Rollback getRollback() {
		return rollback;
	}

	/**
	 * 
	 * @return GoldRush lobby timer class
	 */
	public LobbyTimer getTimer() {
		return timer;
	}

	/**
	 * 
	 * @return GoldRush team & player balances class
	 */
	public BalanceApi getBalance() {
		return balance;
	}

	/**
	 * 
	 * @return GoldRush equipment class
	 */
	public EquipmentManager getEquipmentManager() {
		return equipmentManager;
	}

	/**
	 * Creates and instance of {@link EquipmentManager} and loads all the
	 * pickaxes & armour/weapons into it
	 */
	private void loadEquipments() {
		equipmentManager = new EquipmentManager();
		equipmentManager.registerEquipment(new iKipper_Pick());
		equipmentManager.registerEquipment(new Tailor_Pick());
		equipmentManager.registerEquipment(new Mr_Nut_Pick());
		equipmentManager.registerEquipment(new Gold_Dropper());
		equipmentManager.registerEquipment(new Gold_Destroyer());
		equipmentManager.registerEquipment(new Gold_Picker());
		equipmentManager.registerEquipment(new IronHelmet());
		equipmentManager.registerEquipment(new IronChestplate());
		equipmentManager.registerEquipment(new IronLeggings());
		equipmentManager.registerEquipment(new IronBoots());
		equipmentManager.registerEquipment(new DiamondHelmet());
		equipmentManager.registerEquipment(new DiamondChestplate());
		equipmentManager.registerEquipment(new DiamondLeggings());
		equipmentManager.registerEquipment(new DiamondBoots());
		equipmentManager.registerEquipment(new iKipper_Sword());
		equipmentManager.registerEquipment(new Tailor_Sword());
	}

	/**
	 * Sends the target player to the target server.
	 * 
	 * @param p
	 *            target player
	 * @param server
	 *            target server
	 */
	public void sendPlayer(Player p, String server) {
		Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(instance, "BungeeCord");
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(b);
		try {
			out.writeUTF("Connect");
			out.writeUTF(server);
		} catch (IOException e) {
			e.printStackTrace();
		}
		p.sendPluginMessage(instance, "BungeeCord", b.toByteArray());
	}

	private void setGame(Game game) {
		this.game = game;
	}

	private void setdApi(DatabaseApi dApi) {
		this.dApi = dApi;
	}

	private void setRollback(Rollback rollback) {
		this.rollback = rollback;
	}

	private void setTimer(LobbyTimer timer) {
		this.timer = timer;
	}

	private void setBalance(BalanceApi balance) {
		this.balance = balance;
	}

	public static int debug(String message) {
		return Bukkit.broadcastMessage("[Debug] " + message);
	}
}