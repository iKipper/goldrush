package goldrush.apis;

import java.util.HashMap;

import org.bukkit.entity.Player;

public class BalanceApi {

	private long redBal;
	private long blueBal;
	private HashMap<String, Long> playerBalance;

	public BalanceApi() {
		this.redBal = 0;
		this.blueBal = 0;
		this.playerBalance = new HashMap<String, Long>();
	}

	/**
	 * Sets red teams balance.
	 * 
	 * @param amount
	 *            new red balance
	 */
	public void setRedBalance(long amount) {
		this.redBal = amount;
	}

	/**
	 * Sets blue teams balance.
	 * 
	 * @param amount
	 *            new blue balance
	 */
	public void setBlueBalance(long amount) {
		this.blueBal = amount;
	}

	/**
	 * 
	 * @return red balance
	 */
	public long getRedBalanace() {
		return redBal;
	}

	/**
	 * 
	 * @return blue balance
	 */
	public long getBlueBalance() {
		return blueBal;
	}

	/**
	 * 
	 * @return player balances
	 */
	public HashMap<String, Long> getPlayerBalances() {
		return playerBalance;
	}

	/**
	 * returns the balance of a certain player
	 * 
	 * @param p
	 *            the player
	 * @return
	 */
	public long getPlayerBalance(Player p) {
		return getPlayerBalance(p.getName());
	}

	private long getPlayerBalance(String p) {
		return playerBalance.get(p);
	}
}