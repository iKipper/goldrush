package goldrush.utils;

import goldrush.GoldRush;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Rollback {

	private GoldRush instance;

	public Rollback(GoldRush instance) {
		this.instance = instance;
	}

	/**
	 * Replaces the target world with a fresh version located in the data folder
	 * folder.
	 * 
	 * @param world
	 *            the target world
	 */
	public void rollback(String world) {
		File srcFolder = new File(instance.getDataFolder() + File.separator + world);
		File destFolder = new File(world);
		
		try {
			if (!srcFolder.exists()) {
				System.out.println("World file doesn't exist.");
			} else {
				copyFolder(srcFolder, destFolder);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void copyFolder(File src, File dest) throws IOException {
		if (src.isDirectory()) {
			if(dest.exists()) {
				dest.delete();
				System.out.println("World " + dest.getName() + " deleted."); 
			}
			
			if (!dest.exists()) {
				dest.mkdir();
			}

			String files[] = src.list();

			for (String file : files) {
				File srcFile = new File(src, file);
				File destFile = new File(dest, file);
				copyFolder(srcFile, destFile);
			}
		} else {
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dest);

			byte[] buffer = new byte[1024];

			int length;
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}

			in.close();
			out.close();
			 System.out.println("File copied from " + src + " to " + dest);
		}
	}
}