package goldrush.timers;

import goldrush.GoldRush;

import me.confuser.barapi.BarAPI;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class LobbyTimer implements Runnable {

	private static int timer = 61;
	private static int taskId;
	private static GoldRush instance;
	private static boolean running;

	public LobbyTimer(GoldRush instance) {
		LobbyTimer.instance = instance;
	}

	public void run() {
		timer--;

		for (Player p : Bukkit.getOnlinePlayers()) {
			BarAPI.setMessage(p, ChatColor.GOLD + "GoldRush starts in " + ChatColor.GREEN + timer + ChatColor.GOLD + " second(s) on map " + ChatColor.GREEN + instance.getGame().getMap().getMapName());
		}

		if (timer == 0) {
			stop();

			for (Player p : Bukkit.getOnlinePlayers()) {
				if(Bukkit.getWorld(instance.getGame().getMap().getMapName()) == null) {
					instance.sendPlayer(p, "hub");
					System.out.println("Bad bad bad bug");
				}
				
				BarAPI.removeBar(p);
				instance.getGame().initPlayerScoreboard(p, 0, 0, 10, 10, 0);
				instance.getGame().initPlayerToTeams(p);
				instance.getGame().initPlayerToSpawn(p);
				instance.getGame().initPlayerItems(p);
			}
		}
	}

	/**
	 * Starts the timer.
	 */
	public static void start() {
		taskId = schedule();
		running = true;
	}

	/**
	 * Stops the timer.
	 */
	public static void stop() {
		Bukkit.getScheduler().cancelTask(taskId);
		running = false;
	}

	/**
	 * Schedules a new timer with the taskId.
	 * 
	 * @return timer taskId
	 */
	private static int schedule() {
		return Bukkit.getScheduler().runTaskTimer(instance, new LobbyTimer(instance), 0, 20).getTaskId();
	}

	/**
	 * Set the timer to a new integer.
	 * 
	 * @param timer
	 *            the new timer integer
	 */
	public static void setTimer(int timer) {
		LobbyTimer.timer = timer;
	}

	/**
	 * Returns the currently lobby timer.
	 * 
	 * @return lobby timer
	 */
	public static int getTimer() {
		return timer;
	}

	/**
	 * Checks if the timer is running.
	 * 
	 * @return timer running
	 */
	public static boolean isRunning() {
		return running;
	}

	/**
	 * Checks if the timer is finished.
	 * 
	 * @return timer finished
	 */
	public static boolean isFinished() {
		return timer <= 0;
	}

	/**
	 * Checks if there is enough players to start the game.
	 * 
	 * @return if the game is ready to start
	 */
	public static boolean isReady() {
		return Bukkit.getOnlinePlayers().length >= 1;
	}
}